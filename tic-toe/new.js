var GAME = gameobj => {
    var API = {
        // var game = new Phaser.Game(500, 500, Phaser.AUTO , 'phaser-example', { preload: preload, create: create, update: update }),

//groups and array used to iterate trough during collision checking
        boxGroup:0,
        lines:[],
        link1:gameobj.link1,
        link2:gameobj.link2,


// how manny rows columns to play with and spacing inbetween columns and rows
        gameSize:3,
        spacing:100,

//used for grid size starting position and total width and height.
        // gridSize: API.spacing*API.gameSize,
        topGrid:100,
        leftGrid:100,
        delay:100,


//used to determine who is playing
        playerTwoTurn:false,
        gameText:"",
        board: [0, 1, 2, 3, 4, 5, 6, 7, 8],
        huPlayer : "P",
        aiPlayer : "C",
        iter : 0,
        round : 0,
    
        makeBoxes:(x,y,s,i,scale)=>{

            this.sprite=API.game.add.sprite(x,y,"cell");
            this.sprite.anchor.x=0;
            this.sprite.anchor.y=0;
            this.sprite.scale.setTo(scale);
            this.sprite.inputEnabled = true;
            this.sprite.events.onInputDown.add(API.markIt, this);
            this.sprite.playerset=0;
            this.sprite.alpha=0.01;
            API.boxGroup.add(this.sprite);
            return this.sprite;
        },
        markIt:(object)=>{ 
            numx=(object.x-API.leftGrid)*API.gameSize/API.gridSize
            numy=(object.y-API.topGrid)*API.gameSize/API.gridSize
            if (numy==0)
            {
                var num=numx
            }
            else if (numy==1){
                var num=numx+3
            }
            else if (numy==2){
               var num=numx+6
            }
            console.log(num)
            API.move(num, API.huPlayer,object);
        },

        move:(element, player,object)=> {
          console.log("element"+ element);
          if (API.board[element] != "P" && API.board[element] != "C") {
            API.round++;
            // $(element).css("background-color", color);
            API.board[element] = player;
            console.log(API.board);
            object.x=API.leftGrid+API.gridSize*(numx/API.gameSize)
            object.y=API.topGrid+API.gridSize*(numy/API.gameSize)
            object.inputEnabled=false;      
            console.log(object)
            // pl=create(object.x,object.y,"p3")
            // object.alpha=0;
            
            this.sprite=API.game.add.sprite(object.x,object.y,"p3")
            console.log("eee")
            this.sprite.anchor.x=0;
            this.sprite.anchor.y=0;
            this.sprite.scale.setTo(0.6)
            this.sprite.inputEnabled =false
            this.sprite.alpha=1
            API.boxGroup.add(this.sprite);
            object.tint=" #ff5733";


            if (API.winning(API.board, player)) {
              setTimeout(function() {
                API.succsmsg.text="YOU WIN";
                reset();
              }, 500);
              return;
            } else if (API.round > 8) {
              setTimeout(function() {
                API.drawmsg.text="TIE";
                API.reset();
              }, 500);
              return;
            } else {

              API.round++;
              var index = API.minimax(API.board, API.aiPlayer).index;
               
              API.board[index] = API.aiPlayer;
              if(index<3)
              {
                numy=0
                numx=index
              }
              else if(index<6)
                {
                    numy=1
                    numx=index-3
                }
              else if (index<9)
              {
                numy=2
                numx=index-6
              }
              object.x=API.leftGrid+API.gridSize*(numx/API.gameSize)
              object.y=API.topGrid+API.gridSize*(numy/API.gameSize)
              object.inputEnabled=false; 
              API.game.inputEnabled=false
              setTimeout(function(){API.markai(object)}, API.delay)    
              // markai(object)      

              console.log(API.board);
              console.log(index);
              if (API.winning(API.board, API.aiPlayer)) {
                setTimeout(function() {
                  API.errmsg.text ="YOU LOSE";
                  API.reset();
                }, 500);
                return;
              } else if (API.round === 0) {
                setTimeout(function() {
                  API.drawmsg.text="tie";
                  API.reset();
                }, 500);
                return;
              }
            }
          }
        },

        markai:(object)=>{
            var start = new Date().getTime();
            console.log(start)
            while (new Date().getTime() < start + API.delay);
            API.game.inputEnabled=true
            this.sprite=API.game.add.sprite(object.x,object.y,"x")
            console.log("eee")
            this.sprite.anchor.x=0;
            this.sprite.anchor.y=0;
            this.sprite.scale.setTo(0.6)
            this.sprite.inputEnabled =false
            this.sprite.alpha=1
            API.boxGroup.add(this.sprite);
            // this.sprite.tint=" #f7f9f9 ";
            object.alpha=4
            object.tint=" #ff5733";
        },

        reset:()=>{
            API.round = 0;
            API.board = [0, 1, 2, 3, 4, 5, 6, 7, 8];
            API.boxGroup.removeAll()
        },

        minimax:(reboard, player)=> {
          API.iter++;
          let array = API.avail(reboard);
          if (API.winning(reboard, API.huPlayer)) {
                return {
                  score: -10
                };
            }
            else if (API.winning(reboard, API.aiPlayer)) {
                return {
                  score: 10
                };
            }
            else if (array.length === 0) {
                return {
                  score: 0
                };
            }

            var moves = [];
            for (var i = 0; i < array.length; i++) {
            var move = {};
            move.index = reboard[array[i]];
            reboard[array[i]] = player;

            if (player == API.aiPlayer) {
              var g = API.minimax(reboard, API.huPlayer);
              move.score = g.score;
            } else {
              var g = API.minimax(reboard, API.aiPlayer);
              move.score = g.score;
            }
            reboard[array[i]] = move.index;
            moves.push(move);
          }

          var bestMove;
          if (player === API.aiPlayer) {
            var bestScore = -10000;
            for (var i = 0; i < moves.length; i++) {
              if (moves[i].score > bestScore) {
                bestScore = moves[i].score;
                bestMove = i;
              }
            }
          } else {
            var bestScore = 10000;
            for (var i = 0; i < moves.length; i++) {
              if (moves[i].score < bestScore) {
                bestScore = moves[i].score;
                bestMove = i;
              }
            }
          }
          return moves[bestMove];
    },

//available spots
    avail:(reboard)=> {
        return reboard.filter(s => s != "P" && s != "C");
    },

// winning combinations
    winning:(board, player)=> {
      if (
        (board[0] == player && board[1] == player && board[2] == player) ||
        (board[3] == player && board[4] == player && board[5] == player) ||
        (board[6] == player && board[7] == player && board[8] == player) ||
        (board[0] == player && board[3] == player && board[6] == player) ||
        (board[1] == player && board[4] == player && board[7] == player) ||
        (board[2] == player && board[5] == player && board[8] == player) ||
        (board[0] == player && board[4] == player && board[8] == player) ||
        (board[2] == player && board[4] == player && board[6] == player)
      ) {
        return true;
      } else {
        return false;
      }
    }
    }

// console.log(board)
    API.preload=()=> {
    console.log(API.link1)	
    if (API.link1==null)
    	API.link1="sprite.png"
    if (API.link2==null)
    	API.link2="x.png"
    API.game.load.image('p3', API.link1);
    API.game.load.image('cell', 'cell.png');
    API.game.load.image('level-background','background.png')
    API.game.load.image('x',API.link2)

    }

    API.create=()=> {
        API.gridSize= API.spacing*API.gameSize;
        API.game.add.sprite(0, 0, 'level-background')
        API.errmsg = API.game.add.text(
                API.game.world.centerX,
                API.game.world.centerY,
                "",
                {
                    fontSize:50 + "px",
                    fill: "#990d11",
                    align: "center"
                }
            );
        API.errmsg.anchor = { x: 0.5, y: 0.5 };
        API.errmsg.bringToTop()
        API.succsmsg = API.game.add.text(
                API.game.world.centerX,
                API.game.world.centerY,
                "",
                {
                    fontSize:50 + "px",
                    fill: " #27ae60     ",
                    align: "center"
                }
            );
        API.succsmsg.anchor = { x: 0.5, y: 0.5 };
        API.succsmsg.bringToTop()

        API.drawmsg=API.game.add.text(
                API.game.world.centerX,
                API.game.world.centerY,
                "",
                {
                    fontSize:50 + "px",
                    fill: " #bfc9ca ",
                    align: "center"
                }
            );
        API.drawmsg.anchor = { x: 0.5, y: 0.5 };
        API.drawmsg.bringToTop()

        API.gameText= API.game.add.text(50,50,"Player one your turn!", {
        font: "200 px Arial",
        fill: "#ffffff",
        align: "center"
    });
   
    API.boxGroup=API.game.add.group();
    
    //build the line borders using graphic, not necessary if you have a static tic tac toe background sprite.
            for(s=1;s<API.gameSize;s++){
                var aHLine=API.game.add.graphics(API.leftGrid, API.topGrid+API.gridSize*(s/API.gameSize));
                aHLine.lineStyle(6, 0xffff0, 1);
                aHLine.lineTo(API.gridSize,0);
                aHLine.endFill();
                var aVLine=API.game.add.graphics(API.leftGrid+API.gridSize*(s/API.gameSize), API.topGrid);
                aVLine.lineStyle(6, 0xffff0, 1);
                aVLine.lineTo(0,API.gridSize);
                aVLine.endFill();
            }

    //build the collision boxes, the spacing and gridsize can all be customized:
            for(i=0;i<API.gameSize;i++){
                    for(s=1;s<=API.gameSize;s++){
                        var box=API.makeBoxes(API.leftGrid+API.gridSize*(i/API.gameSize),API.topGrid+API.gridSize*((s-1)/API.gameSize),s,0,0.6);
                    }
            }

    
    }

    API.update=()=> {
	

    }

    API.game = new Phaser.Game(500, 500, Phaser.AUTO , 'phaser-example', { preload: API.preload, create: API.create, update: API.update });
    return API
}