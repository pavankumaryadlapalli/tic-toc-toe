var game = new Phaser.Game(500, 500, Phaser.AUTO , 'phaser-example', { preload: preload, create: create, update: update });

//groups and array used to iterate trough during collision checking
var boxGroup;
var lines=[];

// how manny rows columns to play with and spacing inbetween columns and rows
var gameSize=3;
var spacing=100;

//used for grid size starting position and total width and height.
var gridSize=spacing*gameSize;
var topGrid=100;
var leftGrid=100;
var count=0
var k,l

//used to determine who is playing
var playerTwoTurn=false;
var gameText;
board=createarray(gameSize,gameSize)
// console.log(board)
function preload() {
    // game.load.baseURL = '//examples.phaser.io/';
    // game.load.crossOrigin = 'anonymous';
    //game.load.spritesheet('gameboy', 'assets/sprites/gameboy_seize_color_40x60.png', 40, 60);
    // game.load.image('p3', 'http://examples.phaser.io/assets/sprites/phaser-dude.png');

    game.load.image('p3', 'sprite.png');
}

function create() {

    //   game.add.text(50,50,"Change number of columns/rows and spacing via code", {
    //     font: "100 px Arial",
    //     fill: "#ffffff",
    //     align: "center"
    // });
         gameText= game.add.text(50,50,"Player one your turn!", {
        font: "200 px Arial",
        fill: "#ffffff",
        align: "center"
    });
   
    boxGroup=game.add.group();
    
    //build the line borders using graphic, not necessary if you have a static tic tac toe background sprite.
            for(s=1;s<gameSize;s++){
                var aHLine=game.add.graphics(leftGrid, topGrid+gridSize*(s/gameSize));
                aHLine.lineStyle(6, 0xffff0, 1);
                aHLine.lineTo(gridSize,0);
                aHLine.endFill();
                var aVLine=game.add.graphics(leftGrid+gridSize*(s/gameSize), topGrid);
                aVLine.lineStyle(6, 0xffff0, 1);
                aVLine.lineTo(0,gridSize);
                aVLine.endFill();
            }

    //build the collision boxes, the spacing and gridsize can all be customized:
            for(i=0;i<gameSize;i++){
                    for(s=1;s<=gameSize;s++){
                        var box=new makeBoxes(leftGrid+gridSize*(i/gameSize),topGrid+gridSize*((s-1)/gameSize),s,0,0.6);
                    }
            }

    // build the horizontal and vertical intersection check lines that will be used for collision detection. These are not truely visible, just for calculations.
            for(s=1;s<=gameSize;s++){
                //a horizontal line
                var aHLine=  new Phaser.Line(leftGrid, topGrid+gridSize*(s/gameSize)-(spacing/2), leftGrid+gridSize, topGrid+gridSize*(s/gameSize)-(spacing/2));
                aHLine.intersects=[];
                lines.push(aHLine);
                //a vertical line
                var aVline=new Phaser.Line(leftGrid+gridSize*(s/gameSize)-(spacing/2), topGrid, leftGrid+gridSize*(s/gameSize)-(spacing/2), topGrid+gridSize);
                aVline.intersects=[];
                lines.push(aVline);
            }
  
    // build the cross diagonal intersection lines, basically the parameters are just to give them the right positioning, nothing really special.
    var XLine=  new Phaser.Line(leftGrid, topGrid, leftGrid+gridSize, topGrid+gridSize);
    XLine.intersects=[];
    lines.push(XLine);
  
   //Xline, but just reversed
    var XLine2=  new Phaser.Line(leftGrid, topGrid+gridSize, leftGrid+gridSize,topGrid);
    XLine2.intersects=[];
    lines.push(XLine2);
}

function update() {
	

}

function createarray(no_rows, no_cols){
			var simplearray = [];
			var simplerow = [];
			while (no_cols--) simplerow.push(0);
			while (no_rows--) simplearray.push(simplerow.slice());
			return simplearray;
		}
function render() {
    boxGroup.forEach(function(box){game.debug.body(box)});
    lines.forEach(function(line){game.debug.geom(line)});
}


function makeBoxes(x,y,s,i,scale){

    this.sprite=game.add.sprite(x,y,'p3');
    this.sprite.anchor.x=0;
    this.sprite.anchor.y=0;
    this.sprite.scale.setTo(scale);
    this.sprite.inputEnabled = true;
    this.sprite.events.onInputDown.add(markIt, this);
    this.sprite.playerset=0;
    this.sprite.alpha=0.2;
    boxGroup.add(this.sprite);
    return this.sprite;

}
function ai(object)
{  
	flag=0
	for (i=0;i<3;i++)
	{
		for(j=0;j<3;j++)
		{
			if(board[i][j]==0)
			{ 
				board[i][j]=2
				if(line.intersects.filter(checkP2Combinations).length==gameSize)
					{
						board[i][j]=0
						flag=1
						return i,j
					    
					}
				board[i][j]=0
				
			}
		}
	}
	if (flag==0)
	{
		min=3
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				if (board[i][j]==0)
				{
					board[i][j]=2
					temp=gameSize-line.intersects.filter(checkP1Combinations).length
					console.log(temp)
				    if(temp<min)
					{
						min=temp
						k=i
						l=j
					}
					else board[i][j]=0
							
					
				}
			}
		}
		console.log(k,l)
		return k,l
	}
}
function markIt(object){
	console.log((object.x-leftGrid)*gameSize/gridSize,(object.y-topGrid)*gameSize/gridSize)
    object.inputEnabled=false;		
    object.alpha=1;

    var collidedBox= boxGroup.getChildAt(boxGroup.getIndex(object));
    console.log(collidedBox)
    count=count+1
    
    //the visuals
    if(playerTwoTurn){
    	
		// ds=ai(object)
		// console.log(ds)
        object.tint=0xfff000;
        object.playerset=2;
        numx=(object.x-leftGrid)*gameSize/gridSize
        numy=(object.y-topGrid)*gameSize/gridSize
        board[numy][numx]=2

        console.log	(board)
        gameText.text="player one Your Turn!";

    }else{
    	console.log("sai")
        object.tint=0x000fff;
        object.playerset=1;
        numx=(object.x-leftGrid)*gameSize/gridSize
        numy=(object.y-topGrid)*gameSize/gridSize
        board[numy][numx]=1
        
        console.log(board)	
          gameText.text="player two Your Turn!";

    }
    // console.log("count",count)
    if(count>=gameSize*gameSize){
        stopGame("Draw")
    }
  
  //switch turns
    playerToggler();
    //do the collision checking to decide winner
    collisionCheck(object);
}

function collisionCheck(object){

    //go trough each line and check if it intersects with the clicked area. If it does, for that specific line, let it know that it has player x has intersected it.
    
    lines.forEach(function(line){
            if(Phaser.Line.intersectsRectangle(line,object)){
                if(object.playerset==1){
                    line.intersects.push(1);//push player 1 intersect to the intersects list of this line.
                }
                if(object.playerset==2){
                    line.intersects.push(2);//push player 2 intersect to the intersects list of this line.
                }
                console.log(line.intersects);
                // console.log(line.intersects.filter(checkP1Combinations).length)
                 
                
                    //after each intersection check, check if one of the player has reached a winning matching row/column by counting the number of ones and twos of each line. 
                if(line.intersects.filter(checkP1Combinations).length==gameSize||line.intersects.filter(checkP2Combinations).length==gameSize){

                    if(line.intersects.filter(checkP1Combinations).length==gameSize){
                        stopGame("player 1 Won");//player one wins if we have counted gameSize many ones.
                    }
                    if(line.intersects.filter(checkP2Combinations).length==gameSize){
                        stopGame("player 2 Won");//player one wins if we have counted gameSize many twos.
                    }
                    
                    boxGroup.removeAll();
                    //RESET YOUR GAME HERE
                    //lines.removeAll();
                }
            }
});
}



function checkP1Combinations(player) {
    return player == 1;
}

function checkP2Combinations(player) {
    return player == 2;
}

//used to check if it is player 1 its turn or p2 , if true p2 else p1
function playerToggler(){
    if(playerTwoTurn){
        playerTwoTurn=false;
    }else{
        playerTwoTurn=true;
    }
}

function stopGame(winner){
            gameText.text=winner;

}